﻿using UnityEngine;
using UnityEngine.UI;

public class UIManagment : MonoBehaviour {
	public GameObject landerObject;
	LanderMovement lander;
	Rigidbody2D landerRigid;
	Text scoreText;
	Text fuelText;
	Text timeText;
	Text altitudeText;
	Text verticalSpeedText;
	Text horizontalSpeedText;
	public int m_score;

	float currentTime;
	float minutes;
	float seconds;
	// Use this for initialization
	void Awake () {
		m_score = 0;
		lander = landerObject.GetComponent<LanderMovement> ();
		landerRigid = landerObject.GetComponent<Rigidbody2D> ();
		currentTime = 0;

		//GetTexts
		scoreText=transform.Find ("Panel").Find ("Score").GetComponent<Text>();
		scoreText.text = "Score: "+m_score.ToString ();
		fuelText=transform.Find ("Panel").Find ("Fuel").GetComponent<Text>();
		timeText=transform.Find ("Panel").Find ("Time").GetComponent<Text>();
		altitudeText=transform.Find ("Panel2").Find ("Altitude").GetComponent<Text>();
		verticalSpeedText=transform.Find ("Panel2").Find ("HSpeed").GetComponent<Text>();
		horizontalSpeedText=transform.Find ("Panel2").Find ("VSpeed").GetComponent<Text>();
	}

	void Start(){
		landerObject.GetComponent<LanderLanding> ().scoreEvent.AddListener (NotifyScore);
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;

		//Configure clock format
		minutes = currentTime / 60;
		seconds = currentTime % 60;
		//Setted to the text
		timeText.text = "Time: "+string.Format ("{0:00} : {1:00}", minutes, seconds);
		fuelText.text = "Fuel: "+lander.fuel.ToString ();
		//Panel 2
		altitudeText.text = "Altitude: "+ lander.transform.position.y.ToString("F3");
		verticalSpeedText.text ="Vertical Speed: "+ landerRigid.velocity.y.ToString ("F");
		horizontalSpeedText.text = "Horizontal Speed: "+landerRigid.velocity.x.ToString ("F");
	}
		
	public void NotifyScore(int score){
		m_score += score;
		scoreText.text = "Score: "+m_score.ToString ();
	}

	public void ResetValues(){
		m_score = 0;
		currentTime = 0;
		lander.ResetValues ();
	}

	public int GetScore(){
		return m_score;
	}
}
