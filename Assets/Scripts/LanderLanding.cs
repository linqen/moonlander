﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreIntEvent:UnityEvent<int>{}

public class LanderLanding : MonoBehaviour {
	public float landingMaxVelocity;
	public int scoreForLanding;
	public ScoreIntEvent scoreEvent;
	public UnityEvent onDestroyEvent;
	public int fuelToAddOnLanding;

	void Awake(){
		scoreEvent=new ScoreIntEvent();
	}

	void OnCollisionEnter2D(Collision2D col){
		Debug.Log (transform.eulerAngles.z);
		Debug.Log (col.relativeVelocity.magnitude.ToString());
		if (col.relativeVelocity.magnitude > landingMaxVelocity||transform.eulerAngles.z!=0) {
			Debug.Log ("Destroyed");
			onDestroyEvent.Invoke ();
		} else if(col.gameObject.tag.Equals("Platform")&&transform.eulerAngles.z==0){
			//Send this to the GUI
			int scoreMultipler = col.gameObject.GetComponent<Platform>().scoreMultiply;
			GetComponent<LanderMovement> ().AddFuel (fuelToAddOnLanding*scoreMultipler);
			scoreEvent.Invoke(scoreForLanding*scoreMultipler);
		}

	}
}
