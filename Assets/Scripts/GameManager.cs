﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManager : MonoBehaviour {
	public string mainMenuName;
	public GameObject player;
	public GameObject menuCanvas;
	public GameObject playerCanvas;
	public GameObject GUIcanvas;
	public GameObject eventSystem;
	public GameObject mainCamera;
	public string[] levelNames;
	public float eventTextTime;
	//audio
	public AudioSource gameMusic;
	public AudioSource menuMusic;
	public AudioSource onLandingSound;
	public AudioSource buttonSound;
	public AudioSource explosionSound;
	//private
	private int actualLevel=0;
	private float currentTime;
	private bool hasLanded=false;
	private bool destroyed=false;

	void Awake(){
		DontDestroyOnLoad (gameObject);
		DontDestroyOnLoad (player);
		DontDestroyOnLoad (playerCanvas);
		DontDestroyOnLoad (menuCanvas);
		DontDestroyOnLoad (GUIcanvas);
		DontDestroyOnLoad (eventSystem);
		DontDestroyOnLoad (mainCamera);
	}

	void Start(){
		//PlayerPrefs.DeleteAll ();
		if (PlayerPrefs.GetInt ("audio", 1) == 1) {
			AudioListener.pause = false;
		} else {
			AudioListener.pause = true;
		}
		player.GetComponent<LanderLanding> ().scoreEvent.AddListener (OnLanding);
		player.GetComponent<LanderLanding> ().onDestroyEvent.AddListener (OnDestroyed);
		player.SetActive (false);
		menuMusic.PlayDelayed (1);
	}

	void Update(){
		if (hasLanded) {
			Time.timeScale = 0;
			currentTime += Time.unscaledDeltaTime;
			if (currentTime >= eventTextTime) {
				currentTime = 0;
				HideEventText ();
				Time.timeScale = 1;
				hasLanded = false;
				LoadLevel ();
				player.SetActive (true);
			}
		} else if (destroyed) {
			Time.timeScale = 0;
			currentTime += Time.unscaledDeltaTime;
			if (currentTime >= eventTextTime) {
				currentTime = 0;
				HideEventText ();
				Time.timeScale = 1;
				destroyed = false;
				MainManu ();
			}
		}
	}

	public void MainManu(){
		gameMusic.Stop();
		menuMusic.PlayDelayed (1);
		Destroy(GameObject.Find (levelNames [actualLevel - 1]));
		actualLevel = 0;
		playerCanvas.SetActive (false);
		menuCanvas.SetActive (true);
		player.GetComponent<LanderMovement> ().MainMenu ();
		Resume ();
		HidePause ();
	}

	public void LoadLevel(){
		if (!gameMusic.isPlaying) {
			buttonSound.Play ();
			menuMusic.Stop();
			gameMusic.PlayDelayed (2);
		}
		player.GetComponent<LanderMovement> ().LevelStart ();
		playerCanvas.SetActive (true);
		menuCanvas.SetActive (false);
		try {
			SceneManager.LoadScene(levelNames[actualLevel]);
			actualLevel++;
		} catch{
			actualLevel = 0;
			SceneManager.LoadScene(levelNames[actualLevel]);
			actualLevel++;
		}
		ShowPause ();
	}
	public void ExitGame(){
		buttonSound.Play ();
		Application.Quit ();
	}

	public void BestScores(){
		buttonSound.Play ();
		menuCanvas.transform.Find ("Options").gameObject.SetActive (false);
		Transform scores;
		for (int i = 1; i < 4; i++) {
			int score = PlayerPrefs.GetInt ("Top"+i, 0);
			scores = menuCanvas.transform.Find ("Scores");
			scores.Find (i.ToString()).GetComponent<Text> ().text = score.ToString();
			scores.gameObject.SetActive (true);
			menuCanvas.transform.Find ("Back").gameObject.SetActive (true);
		}
	}

	public void Credits(){
		buttonSound.Play ();
		menuCanvas.transform.Find ("Options").gameObject.SetActive (false);
		menuCanvas.transform.Find ("Credits").gameObject.SetActive (true);
		menuCanvas.transform.Find ("Back").gameObject.SetActive (true);
	}

	public void BackMain(){
		buttonSound.Play ();
		menuCanvas.transform.Find ("Options").gameObject.SetActive (true);
		menuCanvas.transform.Find ("Scores").gameObject.SetActive (false);
		menuCanvas.transform.Find ("Credits").gameObject.SetActive (false);
		menuCanvas.transform.Find ("Back").gameObject.SetActive (false);
	}

	public void OnLanding(int x){
		onLandingSound.Play ();
		ShowEventText("You has Landed, "+x+" points");
		hasLanded = true;
		player.SetActive (false);
	}
	public void OnDestroyed(){
		explosionSound.Play ();
		if (!player.GetComponent<LanderMovement> ().OnCrash ()) {
			ShowEventText ("You has crashed, fueltank has been damaged");
			hasLanded = true;
			player.SetActive (false);
		} else {
			int score = playerCanvas.GetComponent<UIManagment> ().GetScore ();
			ShowEventText("Your Lander has been destroyed, points "+score);
			for (int i = 1; i < 4; i++) {
				int lastScore=PlayerPrefs.GetInt ("Top" + i,0);
				if (lastScore < score) {
					PlayerPrefs.SetInt ("Top" + i, score);
					score = lastScore;
				}
			}
			destroyed = true;
			player.SetActive (false);
		}
	}

	public void Pause(){
		buttonSound.Play ();
		Time.timeScale = 0;
		Transform menu = GUIcanvas.transform.Find ("Pause Menu");
		Transform soundButton = menu.Find ("Sound");
		if (!AudioListener.pause) {
			soundButton.Find("Text").GetComponent<Text> ().text = "Sound On";
		} else {
			soundButton.Find("Text").GetComponent<Text> ().text = "Sound Off";
		}
		menu.gameObject.SetActive (true);
		HidePause ();
	}
	public void ChangeSound(){
		Transform menu = GUIcanvas.transform.Find ("Pause Menu");
		Transform soundButton = menu.Find ("Sound");
		if (!AudioListener.pause) {
			AudioListener.pause=true;
			PlayerPrefs.SetInt ("audio", 0);
			soundButton.Find("Text").GetComponent<Text> ().text = "Sound Off";
		} else {
			AudioListener.pause = false;
			PlayerPrefs.SetInt ("audio", 1);
			soundButton.Find("Text").GetComponent<Text> ().text = "Sound On";
		}
	}
	public void Resume(){
		buttonSound.Play ();
		GUIcanvas.transform.Find ("Pause Menu").gameObject.SetActive (false);
		ShowPause ();
		Time.timeScale = 1;
	}



	private void ShowPause(){
		GUIcanvas.transform.Find ("Pause").gameObject.SetActive (true);
	}
	private void HidePause(){
		GUIcanvas.transform.Find ("Pause").gameObject.SetActive (false);
	}

	private void ShowEventText(string text){
		GUIcanvas.transform.Find ("EventText").gameObject.SetActive (true);
		GUIcanvas.transform.Find ("EventText").GetComponent<Text> ().text = text;
	}
	private void HideEventText(){
		GUIcanvas.transform.Find ("EventText").gameObject.SetActive (false);
	}

}
