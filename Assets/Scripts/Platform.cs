﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {
	public int scoreMultiply;
	TextMesh text;

	void Awake () {
		text = GetComponent<TextMesh> ();
		text.text = "x"+scoreMultiply;
	}
}
