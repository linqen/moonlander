﻿using UnityEngine;
using UnityEngine.Events;
public class LanderMovement : MonoBehaviour {
	//public
	public float propulsionForce;
	public GameObject firePrefab;
	public float rotationVelocity;
	//The delay between every little rotation on seconds
	public float frameRotationDelay;
	public int rotationAngle;
	public int fuel;
	public int losedFuelOnCrash;
	//private
	private int initialFuel;
	Rigidbody2D rb;
	float rotationAxis;
	bool propulsion;
	float zRotation;
	float rotationTime;
	void Awake(){
		rb = GetComponent<Rigidbody2D> ();
		zRotation = transform.eulerAngles.z;
		rotationTime = 0;
		initialFuel = fuel;
	}

	void Update () {
		rotationTime += Time.deltaTime;
		rotationAxis = Input.GetAxisRaw ("Rotation");
		if (rotationAxis != 0 && rotationTime>=frameRotationDelay) {
			zRotation +=rotationAxis* rotationAngle * rotationVelocity;
			zRotation=Mathf.Clamp (zRotation, -90, 90);
			transform.rotation = Quaternion.AngleAxis(zRotation, Vector3.back);
			rotationTime = 0;
		}
		if (Input.GetButton ("Propulsion")&&fuel>0) {
			propulsion = true;
			firePrefab.SetActive (true);
			fuel--;
		} else {
			propulsion = false;
			firePrefab.SetActive (false);
		}
	}

	void FixedUpdate(){
		if (propulsion) {
			rb.AddRelativeForce (Vector3.up * propulsionForce);
		}
	}

	public void LevelStart(){
		transform.position = new Vector2 (0f, 10f);
		zRotation = 0;
		transform.rotation = Quaternion.AngleAxis(0, Vector3.back);
		gameObject.SetActive (true);
	}

	public void AddFuel(int fuelToAddOnLanding){
		fuel += fuelToAddOnLanding;
	}
	//Return true if Lander is death, false if not
	public bool OnCrash(){
		fuel -= losedFuelOnCrash;
		if (fuel <= 0) {
			return true;
		} else {
			return false;
		}
	}

	public void MainMenu(){
		gameObject.SetActive (false);
	}

	public void ResetValues(){
		fuel = initialFuel;
		transform.rotation = Quaternion.AngleAxis(0, Vector3.back);
		zRotation = 0;
		rb.angularVelocity = 0;
	}

}
